const express = require('express');

const router = express.Router();
MongoClient = require('mongodb').MongoClient;
const session = require('express-session');

var bcrypt = require('bcryptjs');

var ObjectId = require('mongodb').ObjectID;

var $ = require("jquery");
const delay = require('delay');

var http = require('http');
var fs = require('fs');

var favoris = []; 

router.get('/', (req,res) => res.render('welcome'));

router.get('/dashboard' , (req,res) => res.render('dashboard',{user:user}));



router.get('/loggin', (req,res) => res.render('loggin'));
router.get('/register', (req,res) => res.render('register'));




// Inscription
router.post('/register', (req,res)=>{
    const { name , email , password , password2 } = req.body;
    let errors = [];

    // check required fields

    if(!name || !email || !password || !password2){
        errors.push({msg: 'Please il manque des champs '});
    }

    // check password match 

    if(password !== password2){
        errors.push({msg: 'la confirmation de mot de passe est incorrect'});
    }

    // ckeck lenght 

    if(password.length < 5){
        errors.push({msg:'la taille du mots de passe est trop court'});
    }

    if(errors.length > 0){
        res.render('register', {
            errors,
            name,
            email,
            password,
            password2
        });
    }
    else{
        MongoClient.connect("mongodb://localhost/test", function(error, db) {
            if (error) return funcCallback(error);
            db.collection("dino").findOne({email:req.body.email}, function(err,result){
                if(err){
                    res.send(err);
                }else{
                    if(result){
                        errors.push({msg:'l email existe deja '});
                        res.render('register', {
                            errors,
                            name,
                            email,
                            password,
                            password2
                        });
                    }
                    else{
                        var passwordhash = req.body.password;
                        bcrypt.genSalt(10,(err,salt)=>
                        bcrypt.hash(passwordhash,salt,(err,hash) => {
                            if(err) throw err;
                            passwordhash = hash;
                            db.collection("dino").insert({name:req.body.name , email:req.body.email, password:passwordhash, date:new Date()});
                            req.flash('success_msg', 'you are now registere and tu peux te logger');
                            res.redirect('/users/loggin'); 
                        }));
                    }
                }   
            });
            
            
        }); 
        
    }
});

// Connexion



router.post('/loggin', function(req, res) {
    MongoClient.connect("mongodb://localhost/test", function(error, db) {
            if (error) return funcCallback(error);
            db.collection("dino").findOne({email:req.body.email}, function(err,user){
                if(!user){
                req.flash('error', 'email nexiste pas');
                res.redirect('/users/loggin'); 
                }
                else{
                    bcrypt.compare(req.body.password,user.password,(err, isMatch)=>{
                        if (err) throw err;
                        if (isMatch) {
                            req.session.email=user._id;
                            res.render('dashboard',{user:user});
                            
                          } else {
                            req.flash('error', 'mot de pass incorrect');
                            res.redirect('/users/loggin'); 
                          }
                    });
                }
            });
    });
    
});




router.get('/logout', (req, res) => {
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/loggin');
  });

  router.get('/informations', (req, res) => {
    console.log(req.session.email);
    MongoClient.connect("mongodb://localhost/test", function(error, db) {
        if (error) return funcCallback(error);
        db.collection('informations').find({id_user:req.session.email}).toArray(function(err,result){
            if(err){
                res.send(err);
            }else{
                res.render('informations', {info:result});
            }   
         });

    });
    
  });


 
  


  router.post('/ajout', (req, res) => {
    console.log(req.session.email);
      MongoClient.connect("mongodb://localhost/test", function(error, db) {
        if (error) return funcCallback(error);
            // attention req.session.email c'est l'id du dino 
            db.collection("informations").insert({age:req.body.Age, id_user:req.session.email, famille:req.body.Famille, race : req.body.Race, nouriture : req.body.Nouriture });
        });  
        res.redirect("javascript:history.go(-1);");
  });



async function asyncForEach(array, callback) {
    for (let index = 0; index <= array.length; index++) {
        if(index < array.length){
            await callback(array[index], index, array);
        }
    }
  }
  router.post('/:id/update', function(req,res,next){
   console.log('test');
   MongoClient.connect("mongodb://localhost/test", function(error, db){
        if (error) return funcCallback(error);
        console.log("test edit " + req.params.id);
        var myquery = { "_id":ObjectId(req.params.id)};
        var newvalues = { $set: {age:req.body.Age, id_user:req.session.email,famille:req.body.Famille, Race : req.body.Race, nouriture : req.body.nouriture }};
        db.collection("informations").updateOne(myquery,newvalues,function(err,resultat){
            if(err)throw err;
            console.log("update reussis");
            res.redirect("javascript:history.go(-1);");
            
        });

    });

});

router.get('/informations/:id/delete', function(req,res,next){
    MongoClient.connect("mongodb://localhost/test", function(error, db){
        if (error) return funcCallback(error);

        console.log("Connecté à la base de données 'testDino'");
        console.log(req.params.id);   
        
        db.collection("informations").remove({"_id":ObjectId(req.params.id)}, function(err,result){
            if(err){
                res.send(err);
            }else{
                res.redirect("javascript:history.go(-1);");
            }
        })

    });
}); 



 router.get('/membre', (req, res) => {
    console.log(req.session.email);
    MongoClient.connect("mongodb://localhost/test", function(error, db) {
        if (error) return funcCallback(error);
        db.collection('dino').find({}).toArray(function(err,result){
            if(err){
                res.send(err);
            }else{
                res.render('membre', {user:result});
            }   
         });

    });
    
  });

 router.post('/:id/membre', function(req, res) {
    MongoClient.connect("mongodb://localhost/test", function(error, db) {
            if (error) return funcCallback(error);
            db.collection("dino").findOne({email:req.body.email}, function(err,user){
                if(!user){
                req.flash('error', 'email nexiste pas');
                res.redirect('/users/membre'); 
                }
                else{
                    console.log(tot);
                    db.collection("dino").update({"_id":ObjectId(req.params.id)},{$push : {amis : req.body.Amis}} , function(err,result){
                     if(err){
                        res.send(err);
                    }
                    else{
                        res.redirect("javascript:history.go(-1);");
                    }
        });


                }
}); 
});
});

router.get('/amis', (req, res) => {
    console.log(req.session.email);
    MongoClient.connect("mongodb://localhost/test", function(error, db) {
        if (error) return funcCallback(error);
        db.collection('dino').find({}).toArray(function(err,result){
            if(err){
                res.send(err);
            }else{
                res.render('amis', {amis:result});
            }   
         });

    });
    
    
});
router.get('/amis/:id/delete', function(req,res,next){
    MongoClient.connect("mongodb://localhost/test", function(error, db){
        if (error) return funcCallback(error);

        console.log("Connecté à la base de données 'testDino'");
        console.log(req.params.id);   
        
        db.collection("dino").remove({"_id":ObjectId(req.params.id)}, function(err,result){
            if(err){
                res.send(err);
            }else{
                res.redirect("javascript:history.go(-1);");
            }
        })

    });
}); 









module.exports = router;